from .models import Stock, Price
from celery.decorators import task
import requests
from celery import shared_task
import time
import os

print('task ok')
#Stock.objects.all().delete()
#Price.objects.all().delete()

def global_quote(API_KEY = os.getenv('DO_ACCESS_APIKEY')):
    #data: {'Note': 'Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day. Please visit https://www.alphavantage.co/premium/ if you would like to target a higher API call frequency.'}
    for s in ["IBM", "BABA", "BAC", "300135.SHZ"]:
        price_response=requests.get('https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol='+s+'&apikey='+API_KEY)
        data = price_response.json()
        #print(data)
        try :
            print(data['Global Quote'])
        except:
            print(data['Note'])
            return 0

        querystock = Stock.objects.all().filter(symbol = s)
        if querystock:
            print("Stock exist")
            insertstock = querystock[0]
        else:
            insertstock = Stock(symbol = data['Global Quote']["01. symbol"])
            insertstock.save()

        dateprice = Price.objects.all().filter(date = data['Global Quote']['07. latest trading day'], stock = insertstock)
        if dateprice:
            print("Price exist")
        else:
            insertprice = Price(open_price = data['Global Quote']['02. open'],
            high_price = data['Global Quote']['03. high'],
            low_price =  data['Global Quote']['04. low'],
            price = data['Global Quote']['05. price'],
            volume = data['Global Quote']['06. volume'],
            date = data['Global Quote']['07. latest trading day'])
            insertprice.stock = insertstock
            insertprice.save()

global_quote()
global_quote()

@task
def global_quote_celery():
    print('tasks fn')


"""
# celery
#global_quote_celery()
#global_quote_celery.delay()
"""